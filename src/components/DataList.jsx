import React, { useState } from 'react';
import DataItem from './DataItem';

function DataList({ dataUser, removeDataitem, addDataItem, updateDataItem }) {
    const [newUser, setNewUser] = useState({
        address: '',
        name: '',
        phone: '',
        postal: '',
    });

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Address</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Postal</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colSpan="6">добавить новую запись</td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="text"
                            placeholder="Address"
                            value={newUser.address}
                            onChange={(e) => {
                                setNewUser({
                                    ...newUser,
                                    address: e.target.value,
                                });
                            }}
                        />
                    </td>
                    <td>
                        <input
                            type="text"
                            placeholder="Name"
                            value={newUser.name}
                            onChange={(e) => {
                                setNewUser({
                                    ...newUser,
                                    name: e.target.value,
                                });
                            }}
                        />
                    </td>
                    <td>
                        <input
                            type="text"
                            placeholder="Phone"
                            value={newUser.phone}
                            onChange={(e) => {
                                setNewUser({
                                    ...newUser,
                                    phone: e.target.value,
                                });
                            }}
                        />
                    </td>
                    <td>
                        <input
                            type="text"
                            placeholder="Postal"
                            value={newUser.postal}
                            onChange={(e) => {
                                setNewUser({
                                    ...newUser,
                                    postal: e.target.value,
                                });
                            }}
                        />
                    </td>
                    <td colSpan="2">
                        <button
                            onClick={() => {
                                addDataItem(newUser);

                                setNewUser({
                                    address: '',
                                    name: '',
                                    phone: '',
                                    postal: '',
                                });
                            }}
                        >
                            Add New
                        </button>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                {dataUser.map((item) => {
                    return (
                        <DataItem
                            key={item._id}
                            item={item}
                            removeDataitem={removeDataitem}
                            updateDataItem={updateDataItem}
                        />
                    );
                })}
            </tbody>
        </table>
    );
}

export default DataList;
