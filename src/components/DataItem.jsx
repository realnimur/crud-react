import React, { useState } from 'react';

function DataItem({ item, removeDataitem, updateDataItem }) {
    const dataCurrentElement = item.data;

    const [userData, setUserData] = useState({
        address: dataCurrentElement.address,
        name: dataCurrentElement.name,
        phone: dataCurrentElement.phone,
        postal: dataCurrentElement.postal,
        inputsReadOnly: true,
        isChange: false,
    });

    return (
        <tr>
            <td>
                <input
                    type="text"
                    readOnly={userData.inputsReadOnly}
                    value={userData.address ? userData.address : 'Пусто'}
                    onChange={(e) => {
                        setUserData({ ...userData, address: e.target.value });
                    }}
                />
            </td>
            <td>
                <input
                    type="text"
                    readOnly={userData.inputsReadOnly}
                    value={userData.name ? userData.name : 'Пусто'}
                    onChange={(e) => {
                        setUserData({ ...userData, name: e.target.value });
                    }}
                />
            </td>
            <td>
                <input
                    type="text"
                    readOnly={userData.inputsReadOnly}
                    value={userData.phone ? userData.phone : 'Пусто'}
                    onChange={(e) => {
                        setUserData({ ...userData, phone: e.target.value });
                    }}
                />
            </td>
            <td>
                <input
                    type="text"
                    readOnly={userData.inputsReadOnly}
                    value={userData.postal ? userData.postal : 'Пусто'}
                    onChange={(e) => {
                        setUserData({ ...userData, postal: e.target.value });
                    }}
                />
            </td>
            <td>
                <button onClick={() => removeDataitem(item._id)}>Delete</button>
            </td>
            <td>
                {!userData.isChange ? (
                    <button
                        onClick={() => {
                            setUserData({
                                ...userData,
                                isChange: true,
                                inputsReadOnly: false,
                            });
                        }}
                    >
                        Change
                    </button>
                ) : (
                    <button
                        onClick={() => {
                            setUserData({
                                ...userData,
                                isChange: false,
                                inputsReadOnly: true,
                            });
                            const { address, name, phone, postal } = userData;
                            updateDataItem(item._id, {
                                address,
                                name,
                                phone,
                                postal,
                            });
                        }}
                    >
                        Save
                    </button>
                )}
            </td>
        </tr>
    );
}

export default DataItem;
