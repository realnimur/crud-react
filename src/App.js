import React, { useEffect, useState } from 'react';
import './App.css';
import DataList from './components/DataList';

const App = () => {
    const [dataUser, setDataUser] = useState([]);

    useEffect(() => {
        fetch('http://178.128.196.163:3000/api/records')
            .then((response) => response.json())
            .then((data) => setDataUser(data));
    }, []);

    const removeDataitem = (id) => {
        fetch(`http://178.128.196.163:3000/api/records/${id}`, {
            method: 'DELETE',
        }).then((response) => {
            if (response.ok) {
                fetch('http://178.128.196.163:3000/api/records')
                    .then((response) => {
                        if (response.ok) {
                            return response.json();
                        }
                    })
                    .then((data) => {
                        return setDataUser(data);
                    });
            } else {
                throw new Error('Failed to delete task');
            }
        });
    };

    const addDataItem = (object) => {
        fetch(`http://178.128.196.163:3000/api/records/`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify({ data: object }),
        }).then((res) => {
            if (res.ok) {
                fetch('http://178.128.196.163:3000/api/records')
                    .then((response) => {
                        if (response.ok) {
                            return response.json();
                        }
                    })
                    .then((data) => {
                        return setDataUser(data);
                    });
            } else {
                throw new Error('Failed to create task');
            }
        });
    };

    const updateDataItem = (id, object) => {
        fetch(`http://178.128.196.163:3000/api/records/${id}`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify({ data: object }),
        }).then((res) => {
            if (res.ok) {
                fetch('http://178.128.196.163:3000/api/records')
                    .then((response) => {
                        if (response.ok) {
                            return response.json();
                        }
                    })
                    .then((data) => {
                        return setDataUser(data);
                    });
            } else {
                throw new Error('Failed to update task');
            }
        });
    };

    return (
        <div className="App">
            {dataUser.length ? (
                <DataList
                    dataUser={dataUser}
                    updateDataItem={updateDataItem}
                    removeDataitem={removeDataitem}
                    addDataItem={addDataItem}
                />
            ) : (
                'Нет данных'
            )}
        </div>
    );
};

export default App;
